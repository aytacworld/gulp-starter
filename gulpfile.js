'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')({lazy: true});
var runSequence = require('run-sequence');
var args = require('yargs').argv;

// region Default

gulp.task('default', ['help']);

gulp.task('help', $.taskListing);

gulp.task('build', function(cb) {
  runSequence(
    'install-bower',
    'lint-js',
    'copy-index',
    'inject-vendor',
    'inject-js',
    'inject-css',
    'install-npm',
    callback(cb));
});

gulp.task('debug', function(cb) {
  runSequence(
    args.live ? 'serve' : 'watch-all',
    callback(cb));
});

gulp.task('deploy', function(cb) {
  var deployOperation = '';
  if (args.stage) {
    deployOperation = 'deploy-stage';
  } else if (args.production) {
    deployOperation = 'deploy-production';
  } else {
    log('environment parameter not set!');
    return cb();
  }
  runSequence(
    'build',
    'optimize',
    deployOperation,
    callback(cb)
  );
});

// endregion Default

// region Watchers

function watchers(reloadMethod) {
  log('watcher: starting');

  gulp.watch([config.files.srcIndex], ['inject-all']);

  gulp.watch([config.files.srcSass], ['inject-css']);

  gulp.watch([config.files.srcHtml], ['angular-templatecache']);

  if (reloadMethod) {
    gulp.watch([config.files.dstIndex], reloadMethod);
  }

  log('watcher: started');
}

gulp.task('watch-all', function() {
  watchers();
});

// endregion Watchers

// region Download/Install

gulp.task('install-bower', ['clean-vendor'], getTask('install-bower', config.installBower));

gulp.task('install-npm', getTask('install-npm', config.installNpm));

// endregion Download/Install

// region Linting

gulp.task('lint-js', getTask('lint-eslint', config.eslint));

// endregion Linting

// region Copying

gulp.task('copy-index', getTask('rename-file', config.copyIndex));

// region .deploy

gulp.task('create-deploy-folder', getTask('copy-files', config.createDeployFolder));

gulp.task('copy-deploy-root', getTask('copy-files', config.deploy.root));

gulp.task('copy-deploy-api', getTask('copy-files', config.deploy.api));

gulp.task('copy-deploy-bin', getTask('copy-files', config.deploy.bin));

gulp.task('copy-deploy-model', getTask('copy-files', config.deploy.model));

gulp.task('copy-deploy-persistence', getTask('copy-files', config.deploy.persistence));

gulp.task('copy-deploy-service', getTask('copy-files', config.deploy.service));

gulp.task('copy-deploy-config', getTask('copy-files', config.deploy.config));

gulp.task('copy-deploy-public', getTask('copy-files', config.deploy.public));

gulp.task('copy-deploy-public-images', getTask('copy-files', config.deploy.publicImages));

gulp.task('copy-deploy-public-fonts', getTask('copy-files', config.deploy.publicFonts));

gulp.task('copy-deploy-public-js', getTask('copy-files', config.deploy.publicJs));

gulp.task('copy-deploy-public-css', getTask('copy-files', config.deploy.publicCss));

// endregion .deploy

// endregion Copying

// region Clean

gulp.task('clean-styles', getTask('remove-files', config.cleanStyles));

gulp.task('clean-vendor', getTask('remove-files', config.cleanVendor));

gulp.task('clean-optimized', getTask('remove-files', config.cleanOptimized));

gulp.task('clean-deploy-public-optimized', getTask('remove-files', config.cleanDeployPublicOptimized));

gulp.task('clean-deploy-folder', ['create-deploy-folder'], getTask('remove-files', config.cleanDeployFolder));

// endregion Clean

// region Inject

gulp.task('angular-templatecache', getTask('angular-templatecache', config.angularTemplatecache));

gulp.task('inject-css', ['compile-sass'], getTask('inject-files', config.injectCss));

gulp.task('inject-js', ['angular-templatecache'], getTask('inject-files', config.injectJs));

gulp.task('inject-vendor', getTask('inject-vendor', config.injectVendor));

gulp.task('inject-all', function(cb) {
  runSequence(
    'copy-index',
    'inject-js',
    'inject-css',
    'inject-vendor',
    callback(cb));
});

// endregion Inject

// region Optimize

gulp.task('optimize', ['clean-optimized'], getTask('optimize-css-js', config.optimize));

// endregion Optimize

// region Compile

gulp.task('compile-sass', ['clean-styles'], getTask('compile-sass', config.compileSass));

// endregion Compile

// region Serve

gulp.task('serve', getTask('serve', serveConfig()));

function serveConfig() {
  var ret = config.serve;

  ret.runWatchers = watchers;

  return ret;
}

// endregion Serve

// region Bump version

gulp.task('bump-patch', getTask('bump', config.bumpPatch));

gulp.task('bump-minor', getTask('bump', config.bumpMinor));

// endregion Bump version

// region Deploy

gulp.task('copy-deploy', function(cb) {
  runSequence(
    'copy-deploy-root',
    'copy-deploy-api',
    'copy-deploy-bin',
    'copy-deploy-model',
    'copy-deploy-persistence',
    'copy-deploy-service',
    'copy-deploy-config',
    'copy-deploy-public',
    'copy-deploy-public-images',
    'copy-deploy-public-fonts',
    'clean-deploy-public-optimized',
    'copy-deploy-public-js',
    'copy-deploy-public-css',
    callback(cb)
  );
});

gulp.task('deploy-stage', ['bump-patch'], function(cb) {
  runSequence(
    'clean-deploy-folder',
    'git-init-staging',
    'git-add-remote-staging',
    args.skipPull ? 'empty-task' : 'git-pull-staging',
    'copy-deploy',
    'git-add-commit-staging',
    'git-push-staging',
    callback(cb)
  );
});

gulp.task('deploy-production', ['bump-minor'], function(cb) {
  runSequence(
    'clean-deploy-folder',
    'git-init-production',
    'git-add-remote-production',
    args.skipPull ? 'empty-task' : 'git-pull-production',
    'copy-deploy',
    'git-add-commit-production',
    'git-push-production',
    callback(cb)
  );
});

// endregion Deploy

// region Git

// region .staging

gulp.task('git-init-staging', getTask('git-init', config.deployStage));

gulp.task('git-add-remote-staging', getTask('git-add-remote', config.deployStage));

gulp.task('git-pull-staging', getTask('git-pull', config.deployStage));

gulp.task('git-add-commit-staging', getTask('git-add-commit', config.deployStage));

gulp.task('git-push-staging', getTask('git-push', config.deployStage));

// endregion .staging

// region .production

gulp.task('git-init-production', getTask('git-init', config.deployProduction));

gulp.task('git-add-remote-production', getTask('git-add-remote', config.deployProduction));

gulp.task('git-pull-production', getTask('git-pull', config.deployProduction));

gulp.task('git-add-commit-production', getTask('git-add-commit', config.deployProduction));

gulp.task('git-push-production', getTask('git-push', config.deployProduction));

// endregion .production

// endregion Git

// region Methods

function  getTask(task, taskConfig) {
  return require('./gulp-tasks/' + task)(gulp, $, taskConfig, log);
}

function log(msg) {
  if (typeof(msg) === 'object') {
    for (var item in msg) {
      if (msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.blue(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(msg));
  }
}

function callback(cb) {
  return function (err) {
    if (err) {
      throw err;
    }
    cb();
  };
}

gulp.task('empty-task', function(cb) {
  cb();
});

// endregion Methods
