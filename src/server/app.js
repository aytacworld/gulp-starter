'use strict';

// region imports

var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');

// endregion imports

// region express config

var app = express();
var config = require('./config/config')(app);

app.use(favicon(config.faviconPath));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(config.publicFolder));

// endregion express config

// region init routes

console.log('no routes defined yet');

// endregion init routes

// region init errorHandlers

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) { // eslint-disable-line
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) { // eslint-disable-line
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

// endregion init errorHandlers

module.exports = app;
