'use strict';
var path = require('path');
var _ = require('underscore');

var clientFolder = path.join(__dirname, '..', 'public');

var configurations = {
  environment: 'development',
  faviconPath: path.join(clientFolder, 'assets', 'images', 'favicon.ico'),
  publicFolder: clientFolder
};

var config = function(app) {
  var env = app.get('env');
  var environmentConfig = {};

  try {
    // try to load local configuration
    environmentConfig = require('./local');
    console.log('Server running in local mode');
  } catch (e) {
    try {
      // if not, try to load environment configuration
      environmentConfig = require('./env/' + env);
      console.log('Server running in ' + env + ' mode');
    } catch (e2) {
      console.log('Server running in config mode');
    }
  }
  _.extend(configurations, environmentConfig);
  return configurations;
};

module.exports = config;
