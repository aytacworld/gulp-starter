# Gulp starter project

This is a starter project. You can just fork it and write your own code on top of it and you can also change the defaults of this project.

There isn't a license added to this project.
You can add a license that's suits your needs. ;)

## Index

1. Gulp tasks
2. Future improvments

## 1. Gulp tasks

### gulp/gulp default/gulp help
Lists all gulp tasks

### gulp build
Will download/install/compile/copy all source files

### gulp debug
Will start watching all source files for changes, and run tasks when changing a watched file

### gulp debug --live
Will start nodemon and livereload and watch source files, will restart/reload server webpage, when changing a source file

### gulp deploy --staging
Runs **gulp build**, then will optimize files and deploy to *staging* environment

### gulp deploy --production
Same as **gulp deploy --staging**, but will deploy to *production* environment

## 2. Future improvments

1. Add testing
2. Write a better README.md
3. ?
