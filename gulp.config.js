'use strict';

module.exports = function() {
  var root = './';
  var src = root + 'src/';
  var srcClient = src + 'client/';
  var srcServer = src + 'server/';
  var srcServerBin = srcServer + 'bin/';
  var deploy = root + '.deploy/';
  var deployClient = deploy + 'public/';

  var files = {
    srcHtml: [srcClient + '**/*.html', '!' + srcClient + 'index.html'],
    srcIndex: [srcClient + 'index.raw.html'],
    srcJS: [
      src + '**/*.js',
      root + 'gulp-tasks/**/*.js',
      root + 'gulp.config.js',
      root + 'gulpfile.js',
      '!**/node_modules/**/*.js',
      '!**/bower_components/**/*.js',
      '!**/.tmp/**/*.js',
      '!' + srcClient + 'lib/**/*.js',
      '!' + srcClient + 'app/app.templates.js',
      '!' + srcClient + 'js/*.js'
    ],
    srcPackages: [
      './package.json',
      './bower.json'
    ],
    srcSass: [srcClient + '**/*.scss', srcClient + '**/*.sass'],

    dstIndex: srcClient + 'index.html',
    dstServerWww: srcServerBin + 'www'
  };

  return {
    angularTemplatecache: {
      src: srcClient + 'app/**/*.html',
      dst: srcClient + 'app/',
      file: 'app.templates.js',
      options: {
        module: 'app.core',
        standAlone: false,
        root: 'app/'
      }
    },
    bumpMinor: {
      src: files.srcPackages,
      dst: root,
      options: {
        type: 'minor'
      }
    },
    bumpPatch: {
      src: files.srcPackages,
      dst: root,
      options: {
        type: 'patch'
      }
    },
    cleanOptimized: {src: [
      srcClient + 'assets/styles/app-*.css',
      srcClient + 'assets/styles/lib-*.css',
      srcClient + 'js/app-*.js',
      srcClient + 'js/lib-*.js',
      srcClient + 'rev-manifest.json'
    ]},
    cleanDeployFolder: {src: [deploy + '*', deploy + '.*', deploy + '.git/**']},
    cleanDeployPublicOptimized: {src: [
      deployClient + 'assets/styles/*.css',
      deployClient + 'js/*.js'
    ]},
    cleanStyles: {src: [srcClient + 'assets/**/*.css', srcClient + 'styles/*.css']},
    cleanVendor: {src: [srcClient + 'lib/*', srcClient + 'lib/.*']},
    compileSass: {src: files.srcSass, dst: srcClient},
    copyIndex: {src: files.srcIndex, dst: srcClient, newName: 'index.html'},
    createDeployFolder: {src: files.srcIndex, dst: deploy},
    deploy: {
      root: {
        src: [
          srcServer + 'app.js',
          srcServer + 'package.json'
        ],
        dst: deploy
      },
      api: {
        src: [srcServer + 'api/**/*', srcServer + 'api/**/.*'],
        dst: deploy + 'api/'
      },
      bin: {
        src: [srcServer + 'bin/www'],
        dst: deploy + 'bin/'
      },
      model: {
        src: [srcServer + 'model/**/*', srcServer + 'model/**/.*'],
        dst: deploy + 'model/'
      },
      persistence: {
        src: [srcServer + 'persistence/**/*', srcServer + 'persistence/**/.*'],
        dst: deploy + 'persistence/'
      },
      service: {
        src: [srcServer + 'service/**/*', srcServer + 'service/**/.*'],
        dst: deploy + 'service/'
      },
      config: {
        src: [srcServer + 'config/**/*', srcServer + 'config/**/.*', '!' + srcServer + 'config/local.js'],
        dst: deploy + 'config/'
      },
      public: {
        src: [srcClient + 'index.html'],
        dst: deployClient
      },
      publicFonts: {
        src: [
          srcClient + 'assets/fonts/*',
          srcClient + 'assets/fonts/.*',
          srcClient + 'lib/bootstrap-css/fonts/*'
        ],
        dst: deployClient + 'assets/fonts/'
      },
      publicImages: {
        src: [srcClient + 'assets/images/*'],
        dst: deployClient + 'assets/images'
      },
      publicCss: {
        src: [srcClient + 'assets/styles/lib-*.css', srcClient + 'assets/styles/app-*.css'],
        dst: deployClient + 'assets/styles'
      },
      publicJs: {
        src: [srcClient + 'js/lib-*.js', srcClient + 'js/app-*.js'],
        dst: deployClient + 'js'
      }
    },
    deployProduction: {
      src: [
        deploy + '*'
      ],
      cwd: '.deploy',

      repository: 'origin',
      branch: 'master',
      remote: '***' // git url to production repository
    },
    deployStage: {
      src: [
        deploy + '*'
      ],
      cwd: '.deploy',

      repository: 'origin',
      branch: 'master',
      remote: '***' // git url to staging repository
    },
    eslint: {src: files.srcJS},
    files: files,
    injectCss: {
      src: files.dstIndex,
      dst: srcClient,
      appFiles: [
        srcClient + 'assets/styles/*.css'
      ]
    },
    injectJs: {
      src: files.dstIndex,
      dst: srcClient,
      appFiles: [
        srcClient + 'app/translations/*.js',
        srcClient + 'app/models/*.js',
        srcClient + 'app/**/*.module.js',
        srcClient + 'app/shared/**/*.service.js',
        srcClient + 'app/**/*.js'
      ]
    },
    injectVendor: {
      src: files.dstIndex,
      dst: srcClient,
      options: {
        bowerJson: require(srcClient + 'bower.json'),
        directory: srcClient + 'lib/',
        ignorePath: '../..'
      }
    },
    installBower: {dst: srcClient + 'lib', options: {cwd: srcClient}},
    installNpm: {src: srcServer + 'package.json'},
    optimize: {
      src: files.dstIndex,
      dst: srcClient,
      optimized: {
        app: 'app.js',
        lib: 'lib.js'
      }
    },
    serve: {
      nodemonOptions: {
        script: files.dstServerWww,
        delayTime: 1,
        ext: 'js',
        watch: [srcServer],
        ignore: ['node_modules']
      }
    }
  };
};
