'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Optimizing the js, css, html');

    var assets = plugins.useref.assets({searchPath: config.dst});
    var cssFilter = plugins.filter('**/*.css', {restore: true});
    var jsLibFilter = plugins.filter('**/' + config.optimized.lib, {restore: true});
    var jsAppFilter = plugins.filter('**/' + config.optimized.app, {restore: true});

    return gulp.src(config.src)
      .pipe(plugins.plumber())
      .pipe(assets)
      .pipe(cssFilter)
      .pipe(plugins.csso())
      .pipe(cssFilter.restore)
      .pipe(jsLibFilter)
      .pipe(plugins.uglify())
      .pipe(jsLibFilter.restore)
      .pipe(jsAppFilter)
      .pipe(plugins.ngAnnotate())
      .pipe(plugins.uglify())
      .pipe(jsAppFilter.restore)
      .pipe(plugins.rev())
      .pipe(assets.restore())
      .pipe(plugins.useref())
      .pipe(plugins.revReplace())
      .pipe(gulp.dest(config.dst))
      .pipe(plugins.rev.manifest())
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
