'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Copying files');

    return gulp.src(config.src)
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
