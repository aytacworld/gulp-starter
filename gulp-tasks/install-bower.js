'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Install bower packages');

    return plugins.bower(config.options)
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
