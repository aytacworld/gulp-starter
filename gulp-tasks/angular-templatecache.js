'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Creating AngularJS $templateCache');

    return gulp.src(config.src)
      .pipe(plugins.minifyHtml({empty: true}))
      .pipe(plugins.angularTemplatecache(config.file, config.options))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
