'use strict';

function task(gulp, plugins, config, log) {
  return function(cb) {
    log('Git Init');

    plugins.git.init({cwd: config.cwd}, function(err) {
      if (err) { throw err; }
      cb();
    });
  };
}

module.exports = task;
