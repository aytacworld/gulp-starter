'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Copying and compressing the images');

    return gulp.src(config.src)
      .pipe(plugins.imagemin({optimizationLevel: 4}))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
