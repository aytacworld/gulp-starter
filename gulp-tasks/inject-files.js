'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Wire up the app files into the html');

    return gulp
      .src(config.src)
      .pipe(plugins.inject(gulp.src(config.appFiles), {relative: true}))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
