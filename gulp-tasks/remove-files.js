'use strict';
var del = require('del');

function task(gulp, plugins, config, log) {
  return function(cb) {
    log('Clear directory');

    del.sync(config.src);
    cb();
  };
}

module.exports = task;
