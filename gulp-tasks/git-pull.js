'use strict';

function task(gulp, plugins, config, log) {
  return function(cb) {
    log('Git Pull');

    plugins.git.pull(config.repository, config.branch, {cwd: config.cwd}, function(){
      cb();
    });
  };
}

module.exports = task;
