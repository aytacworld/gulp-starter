'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('running debug command');
    var alreadyStarted = false;

    plugins.livereload.listen({start: true});

    //configure nodemon
    plugins.nodemon(config.nodemonOptions)
      .on('start', function() {
        log('nodemon started');
        if (!alreadyStarted) {
          config.runWatchers(plugins.livereload.reload);
          alreadyStarted = true;
        } else {
          plugins.livereload.reload();
        }
      });
  };
}

module.exports = task;
