'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Rename files');

    return gulp.src(config.src)
      .pipe(plugins.rename(config.newName))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
