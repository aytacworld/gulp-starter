'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Wire up the bower css js');
    var wiredep = require('wiredep').stream;

    return gulp
      .src(config.src)
      .pipe(wiredep(config.options))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
