'use strict';

/**
 * Bump the version
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and ignore other flags
 */

function task(gulp, plugins, config, log) {
  return function() {
    var msg = 'Bumping versions';
    if (config.options.version) {
      msg += ' to ' + config.options.version;
    } else {
      msg += ' for a ' + config.options.type;
    }
    log(msg);

    return gulp.src(config.src)
      .pipe(plugins.bump(config.options))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
