'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Analyzing source with ESLint');

    return gulp.src(config.src)
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format())
      .pipe(plugins.eslint.failOnError());
  };
}

module.exports = task;
