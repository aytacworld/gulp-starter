'use strict';

function task(gulp, plugins, config, log) {
  return function(cb) {
    log('Git Push');

    plugins.git.push(config.repository, config.branch, {cwd: config.cwd}, function(err) {
      if (err) { throw err; }
      cb();
    });
  };
}

module.exports = task;
