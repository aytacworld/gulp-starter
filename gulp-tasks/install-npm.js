'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Installing NPM packages');

    return gulp.src(config.src)
      .pipe(plugins.install());
  };
}

module.exports = task;
