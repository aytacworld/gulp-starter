'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Compiling Sass --> CSS');

    return gulp
      .src(config.src)
      .pipe(plugins.plumber())
      .pipe(plugins.sass())
      .pipe(plugins.autoprefixer({browsers: ['last 2 version', '> 5%']}))
      .pipe(gulp.dest(config.dst));
  };
}

module.exports = task;
