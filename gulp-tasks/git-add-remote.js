'use strict';

function task(gulp, plugins, config, log) {
  return function(cb) {
    log('Git Add Remote');

    plugins.git.addRemote(config.repository, config.remote, {cwd: config.cwd}, function(err) {
      if (err) { throw err; }
      cb();
    });
  };
}

module.exports = task;
