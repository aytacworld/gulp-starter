'use strict';

function task(gulp, plugins, config, log) {
  return function() {
    log('Git Add/Commit');

    return gulp.src(config.src)
      .pipe(plugins.git.add({args: '-f', cwd: config.cwd}))
      .pipe(plugins.git.commit('production', {cwd: config.cwd}));
  };
}

module.exports = task;
